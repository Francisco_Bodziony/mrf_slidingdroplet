/*---------------------------------------------------------------------------* \
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    calcGlobalSherwoodFromAverageConcentration

Authors
    Daniel Deising     <deising@mma.tu-darmstadt.de>
    All rights reserved.

Description
    Calculates the global Sherwood number based on depletion of average 
    bubble (assumes alpha1=1 within bubble) concentration between available 
    time directories. Also writes out average bubble concentration.

    You may refer to this software as :
    //- Deising et. al. 2016, Chem. Eng. Sci. 139, pp. 173-195

    This code has been developed by :
        Daniel Deising (main developer).
    
    Method Development and Intellectual Property :
        Daniel Deising   <deising@mma.tu-darmstadt.de>
        Holger Marschall <marschall@mma.tu-darmstadt.de>
        Dieter Bothe     <bothe@mma.tu-darmstadt.de>

        Mathematical Modeling and Analysis
        Center of Smart Interfaces
        Technische Universitaet Darmstadt
       
    If you use this software for your scientific work or your publications,
    please don't forget to acknowledge explicitly the use of it.

\*---------------------------------------------------------------------------*/

#include "fvc.H"
#include "OFstream.H"
#include "fvCFD.H"
#include "argList.H"
#include "dynamicFvMesh.H"

#include "TwoPhaseMixture.H"
#include "IOobjectList.H"
#include "speciesTransfer.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    // add arguments to function call:
    Foam::timeSelector::addOptions();
#   include "addRegionOption.H"
    argList::validOptions.insert("write", "");
    argList::validOptions.insert("mrf", "");

    // create list of time dirs
#   include "setRootCase.H"
#   include "createTime.H"

    instantList timeDirs = timeSelector::select0(runTime, args);

    #include "createDynamicFvMesh.H"
    
    // read updated mesh
    mesh.readUpdate();

    // read arguments if present
    bool writeResults = args.optionFound("write");
    bool isMRF = args.optionFound("mrf");

    #include "getNSpecies.H"

    // loop over all time dirs found:
    forAll(timeDirs, timeI)
    {
        // create/open file averageSpeciesConcentration.dat
        ofstream fout("globalSherwoodFromAverageConcentration.dat", ios_base::app);
        if (timeI == 0)
        {
            fout<< "# time \t average bubble velocity (y) \t bubble surface area \t average species concentration (C0,C1,...) \n" << endl;
        }

        dimensionedScalar tNew("tNew", dimTime, 0.);
        dimensionedScalar tOld("tOld", dimTime, 0.);
        if (timeI > 0)
        {
            runTime.setTime(timeDirs[timeI-1], timeI-1);
            //Info<< "Time 1 = " << runTime.time() << endl;
            tOld = runTime.time();
        }

        runTime.setTime(timeDirs[timeI], timeI);
        tNew = runTime.time();
        Info<< "Time = " << runTime.timeName() << endl;
        
        dimensionedScalar deltaT = tNew - tOld;
        Info<< "delta t = " << deltaT << endl;

        #include "readFields.H"
        #include "readSpecies.H"
        #include "calcRiseVelocity.H"

        // same implementation as in calculateGlobalSherwood.H
        volScalarField alphaCut = pos(alpha1 - scalar(0.9));

        for (label i=0; i < C.size(); i++)
        {
            CiAveOld[i] = CiAve[i];
            CiAve[i] = C[i].weightedAverage(alphaCut*mesh.Vsc());
        }

        dimensionedScalar bubbleArea = gSum(mag(fvc::grad(alpha1)) * mesh.V());
        dimensionedScalar bubbleVolume("bubbleVolume", pos(alpha1 - scalar(0.9))().weightedAverage(mesh.V()) * gSum(mesh.V()));
        Info<< "bubble area: " <<  bubbleArea.value() << endl;
        Info<< "bubble volume: " <<  bubbleVolume.value() << endl;

        if (isMRF)
        {
            fout << runTime.value() << "\t" << sumField.component(1).value() << "\t" << bubbleArea.value();
        }
        else
        {
            fout << runTime.value() << "\t" << Ugas.component(1).value() << "\t" << bubbleArea.value();
        }

        for (label i=0; i < C.size(); i++)
        {
            fout<< "\t" << CiAve[i].value();
                
            if (timeI > 0)
            {
            //calculate Ci dot (time derivative of average bubble concentration)
                dimensionedScalar deltaCi = CiAveOld[i] - CiAve[i];
                dimensionedScalar ddtCi = deltaCi/deltaT;
            
            // calculate liquid-sided concentration at interface
                // version 1: based on average species concentration in the bubble (only valid for Dgas >> Dliq)
                dimensionedScalar CiIliq = (CiAveOld[i] + CiAve[i])/2;
                if (He[i].value() > 0)
                {
                    CiIliq = CiIliq / He[i];
                }
                if (Dliq[i].value() > 0 && CiIliq.value() > 0)
                {
                    Sh[i] = (ddtCi/CiIliq * bubbleVolume/bubbleArea) * bubbleDiam / Dliq[i] * dimensionedScalar("fac", dimLength, 1.);
                }
                else
                {
                    Sh[i] = 0;
                }
                Info<< "Sherwood global (C" << i << ") = " << Sh[i].value() << endl;
                fout<< "\t" << Sh[i].value();
            }
        }
        fout<< "\n";

        if (writeResults)
        {
            Info<< "    Calculating velocity Field relative to the bubble motion " << endl;
        }
    }// end loop time dirs

    Info<< "\nEnd\n" << endl;

    return 0;
}


// ************************************************************************* //
