

// define (and initialize) variables needed for MRF/IRF
// 
// the fields starting with 's' are start-up values which are read -if present- 
// from restart.raw-file in the time folder

    // Velocity field in the moving reference frame
    volVectorField Umrf
    (
        IOobject
        (
            "Umrf",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh, 
        dimensionedVector("Umrf", dimVelocity, Foam::vector(0,0,0))
    );

    // PID-Controller error
    dimensionedVector sError("sError", dimLength, Foam::vector(0,0,0));

    // current bubble centre position
    dimensionedVector sCentre("sCentre", mesh.C().weightedAverage(alpha1*mesh.V()));

    // current bubble velocity (set to be the velocity of the moving frame) (=- sBubbleVel)
    dimensionedVector sInletVel("sInletVel", dimVelocity, Foam::vector(0,0,0));

    // current bubble velocity (relative to IRF - inertial reference frame)
    dimensionedVector sBubbleVel("sBubbleVel", dimVelocity, Foam::vector(0,0,0));

    // current bubble velocity (relative to MRF - moving reference frame)
    dimensionedVector sBubbleRelVel("sBubbleRelVel", dimVelocity, Foam::vector(0,0,0));

    // current adjustment velocity to keep bubble in fixed position on the grid (calculated from PIDController.H)
    dimensionedVector sUAdjust("sUAdjust", dimVelocity, Foam::vector(0,0,0));

    // integral corrector of PID controller
    dimensionedVector sIntegralComponent("sIntegralComponent",dimensionSet(0,1,1,0,0), Foam::vector(0,0,0));

    // current bubble acceleration (relative to IRF)
    dimensionedVector sBubbleAcc("sBubbleAcc", dimVelocity/dimTime, Foam::vector(0,0,0));

    // current bubble acceleration (relative to MRF)
    dimensionedVector sBubbleRelAcc("sBubbleRelAcc", dimVelocity/dimTime, Foam::vector(0,0,0));

    // current MRF acceleration (compared to IRF)
    dimensionedVector sFrameAcc("sFrameAcc", dimVelocity/dimTime, Foam::vector(0,0,0));

    // current velocity of the MRF (relative to IRF) (= sInletVel)
    dimensionedVector sFrameVel("sFrameVel", dimVelocity, Foam::vector(0,0,0));

    // current position of the MRF (relative to IRF)
    dimensionedVector sFramePos("sFramePos", dimLength, Foam::vector(0,0,0));

    // current position of the bubble barycentre 
    // bubble position at simulation start is set to be the target position!!!
    dimensionedVector sCentreTarget("sCentreTarget", sCentre);


// update variable values if restart.raw file is present in time folder
#   include "readRestartMRFIRF.H"


