
// compute fluxes needed for advection of alpha and Ci

surfaceScalarField phic = mag((phi - (inletVel & mesh.Sf()))/mesh.magSf());
phic = min(twoPhaseProperties.cAlpha()*phic, max(phic));
surfaceScalarField phir = phic*twoPhaseProperties.nHatf();
surfaceScalarField phiAlphaEqn = phi;
surfaceScalarField comprFlux = phi;

// init flux for alpha-advection:
if (mrfModel == "IRF" || mrfModel == "irf")
{
    phic = mag(phi/mesh.magSf()) + mag(phiFrameVel/mesh.magSf());
    phiAlphaEqn += phiFrameVel;
}

Info << " computing fluxes for mrf " << endl;
Info << " COMPUTING FLUXES FOR MRF " << endl;
