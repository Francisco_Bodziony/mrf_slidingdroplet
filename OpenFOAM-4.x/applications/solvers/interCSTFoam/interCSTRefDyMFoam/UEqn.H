Info<<"UEqn.H interCSTRefFoam " << endl;
    // implementation analogous to divDevRhoRef() in turbulence models
    volScalarField muEffv
    (
        "muEffv",
        twoPhaseProperties.mu()
    );

    fvVectorMatrix UEqn
    (
        fvm::ddt(rho, U)
      + fvm::div(rhoPhi, U)
      - fvc::div((muEffv)*dev2(T(fvc::grad(U))))
      - fvm::laplacian(muEffv, U)
    );

    if (mrfModel == "MRF" || mrfModel == "mrf")
    {
        UEqn = (UEqn + rho*frameAcc);
    }

    if (pimple.momentumPredictor())
    {
        solve
        (
            UEqn
         ==
            fvc::reconstruct
            (
                (
                    twoPhaseProperties.surfaceTensionForce()
                  - ghf*fvc::snGrad(rho)
                  - fvc::snGrad(p_rgh)
                ) * mesh.magSf()
            )
        );
    }
