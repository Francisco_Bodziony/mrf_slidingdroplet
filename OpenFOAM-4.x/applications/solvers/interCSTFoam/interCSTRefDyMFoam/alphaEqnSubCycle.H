label nAlphaCorr(readLabel(pimple.dict().lookup("nAlphaCorr")));
label nAlphaSubCycles(readLabel(pimple.dict().lookup("nAlphaSubCycles")));

if (nAlphaSubCycles > 1)
{
    dimensionedScalar totalDeltaT = runTime.deltaT();
    surfaceScalarField rhoPhiSum(0.0*rhoPhi);

    for
    (
        subCycle<volScalarField> alphaSubCycle(alpha1, nAlphaSubCycles);
        !(++alphaSubCycle).end();
    )
    {
        word alphaScheme("div(phi,alpha)");
        word alpharScheme("div(phirb,alpha)");

        #include "computeAdvectionFluxes.H"
        #include "alphaEqn.H"
        etaF = twoPhaseProperties.etaf();
        #include "cEqn.H"
        rhoPhiSum += (runTime.deltaT()/totalDeltaT)*rhoPhi;
    }

    rhoPhi = rhoPhiSum;
}
else
{
    word alphaScheme("div(phi,alpha)");
    word alpharScheme("div(phirb,alpha)");

    #include "computeAdvectionFluxes.H"
    #include "alphaEqn.H"
    etaF = twoPhaseProperties.etaf();
    #include "cEqn.H"
}

rho == alpha1*rho1 + (scalar(1) - alpha1)*rho2;
