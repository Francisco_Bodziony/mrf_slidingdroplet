    Info<< "Reading field p_rgh\n" << endl;
    volScalarField p_rgh
    (
        IOobject
        (
            "p_rgh",
            runTime.timeName(),
            mesh,
            IOobject::MUST_READ,
            IOobject::AUTO_WRITE
        ),
        mesh
    );

    Info<< "Reading field U\n" << endl;
    volVectorField U
    (
        IOobject
        (
            "U",
            runTime.timeName(),
            mesh,
            IOobject::MUST_READ,
            IOobject::AUTO_WRITE
        ),
        mesh
    );

    #include "createPhi.H"

    Info<< "Reading field alpha1\n" << endl;
    volScalarField alpha1
    (
        IOobject
        (
            "alpha1",
            runTime.timeName(),
            mesh,
            IOobject::MUST_READ,
            IOobject::AUTO_WRITE
        ),
        mesh
    );
    
    Info<< "Creating field etaF\n" << endl;
    surfaceScalarField etaF
    (
        IOobject
        (
            "etaF",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ
        ),
        mesh,
        dimensionedScalar("etaF", dimless, 0.0)
    );

    Info<< "Reading transportProperties\n" << endl;
    TwoPhaseMixture twoPhaseProperties(U, phi);

    IOdictionary transportProperties
    (
        IOobject
        (
            "transportProperties",
            runTime.constant(),
            runTime,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    );
    
    etaF = twoPhaseProperties.etaf();
    const surfaceScalarField& isInterfaceF = twoPhaseProperties.isInterfacef();

    const dimensionedScalar& rho1 = twoPhaseProperties.rho1();
    const dimensionedScalar& rho2 = twoPhaseProperties.rho2();

    // Need to store rho for ddt(rho, U)
    volScalarField rho
    (
        IOobject
        (
            "rho",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT
        ),
        alpha1*rho1 + (scalar(1) - alpha1)*rho2,
        alpha1.boundaryField().types()
    );
    rho.oldTime();


    // Mass flux
    // Initialisation does not matter because rhoPhi is reset after the
    // alpha1 solution before it is used in the U equation.
    surfaceScalarField rhoPhi
    (
        IOobject
        (
            "rho*phi",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        rho1*phi
    );

    // use of turbulence model currently not working due to own twoPhaseProperties-class
    // Construct incompressible turbulence model
    //autoPtr<incompressible::turbulenceModel> turbulence
    //(
    //    incompressible::turbulenceModel::New(U, phi, twoPhaseProperties)
    //);

    #include "readGravitationalAcceleration.H"

    Info<< "Calculating field g.h\n" << endl;
    volScalarField gh("gh", g & mesh.C());
    surfaceScalarField ghf("ghf", g & mesh.Cf());

    IOdictionary fvSchemes
    (
        IOobject
        (
            "fvSchemes",
            runTime.system(),
            runTime,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    );


    Switch useMULES("no");
    if (fvSchemes.subDict("divSchemes").found("useMULES"))
    {
        useMULES = fvSchemes.subDict("divSchemes")["useMULES"][0].wordToken();
    }
    Info<< " use MULES " << useMULES << endl;


    // for consistent advection (set to alpha1.oldTime())
    volScalarField alphaSpec
    (
        IOobject
        (
            "alphaSpec",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        alpha1
    );
    volScalarField alphaSpec2
    (
        IOobject
        (
            "alphaSpec2",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        scalar(1.) - alpha1
    );
    
    // get MULES limiter field for consistent advection
    surfaceScalarField mulesLimiter
    (
        IOobject
        (
            "mulesLimiter",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::NO_WRITE //AUTO_WRITE
        ),
        fvc::interpolate(alpha1)*0.
    );


    // flag for standard Crank-Nicolson treatment of alphaEqn and cEqn!
    Switch useCN("no");
    if (fvSchemes.subDict("ddtSchemes").found("useCN"))
    {
        useCN = fvSchemes.subDict("ddtSchemes")["useCN"][0].wordToken();
    }
    Info<< " use standard Crank-Nicolson scheme: " << useCN << endl;

