

    IOdictionary speciesProperties
    (
        IOobject
        (
            "speciesProperties",
            runTime.constant(),
            runTime,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    );

// Determine number of species concentration fields
scalar n=0;

// Search for list of objects at startTime
//IOobjectList objects(mesh,"0");  
IOobjectList objects(mesh, runTime.timeName()); // better for restart!

// Search list of objects for volScalarFields
IOobjectList scalarFields
(
    objects.lookupClass("volScalarField")
);

for
(
    IOobjectList::iterator scalarFieldIter 
    = scalarFields.begin();
    scalarFieldIter != scalarFields.end();
    ++ scalarFieldIter
)
{
    // Read field
    volScalarField field
    (
        *scalarFieldIter(),
         mesh
    );
    word fieldname = field.name();
    if (fieldname.find("C")==0 && fieldname.size()<=3)
    {
        n++;
    }
}
Info<< "Number of Species/ "<< n << endl;

// Create species concentration fields
PtrList<volScalarField> C(n);

// Create fields for species concentration residuals
//PtrList<volScalarField> Cres(n);

// Create fields to quantify interfacial species transfer
// local Sherwood number (face-centred)
//PtrList<surfaceScalarField> ShLocF(n);

for (label i=0; i < C.size(); i++)
{
    word fieldName = "C" + Foam::name(i);
    Info<< "Reading field" << fieldName << endl;
    C.set 
    (
        i,
        new volScalarField
        (
            IOobject
            (
                fieldName,
                runTime.timeName(),
                mesh,
                IOobject::MUST_READ,
                IOobject::AUTO_WRITE
            ),
            mesh
        )
    );
}
/*
// added for testing ---------------------------
    volScalarField alphaS = alpha1;
{
    // elliptical relaxation:
    scalar smoothFactor = 1.5; // parameter sets the width of smearing
    dimensionedScalar DAlpha = min(pow(smoothFactor/alpha1.mesh().deltaCoeffs(),2));
    fvScalarMatrix alphaSEqn
    (
        fvm::Sp(scalar(1),alphaS) - fvm::laplacian(DAlpha,alphaS) == alpha1
    );
    alphaSEqn.solve(mesh.solver("alphaS"));
}
// ----------------------------------------------
*/
    //const surfaceScalarField& myetaf = continuousInterface.etaf();
    // test new speciesTransfer-Implementation
    speciesTransfer specTrans(alpha1, etaF); //use smooth alpha for testing local sherwood profiles

    // add chemical reactions:
    IOdictionary mixtureProperties
    (
        IOobject
        (
            "mixtureProperties",
            runTime.constant(),
            runTime,
            IOobject::READ_IF_PRESENT,
            IOobject::NO_WRITE
        )
    );
    IOdictionary reactions
    (
        IOobject
        (
            "reactions",
            runTime.constant(),
            runTime,
            IOobject::READ_IF_PRESENT,
            IOobject::NO_WRITE
        )
    );
    IOdictionary chemistryProperties
    (
        IOobject
        (
            "chemistryProperties",
            runTime.constant(),
            runTime,
            IOobject::READ_IF_PRESENT,
            IOobject::NO_WRITE
        )
    );

    // create empty pointers for chemistry models
    chemistryModel* chemistry(NULL);
    reactiveMixture* reaction(NULL);
    bool useChemistry(false);

    // check if chemistry dictionaries are present
    if (!(mixtureProperties.headerOk() && reactions.headerOk() && chemistryProperties.headerOk()))
    {
        Info << "not all chemistry dictionaries found! Chemistry is disabled ... " << endl; 
    }
    else
    {
        // use chemistry model
        useChemistry=true;

        // create reactiveMixture class
        reaction = 
        (
            new reactiveMixture
            (
                mixtureProperties, 
                C, 
                mesh
            )
        );
    
        // Create chemical reactions
        Info << "Constructing chemical mechanism" << endl;
        chemistry =
        (
            new chemistryModel
            (
                mesh,
                *reaction
            )
        );
    }


    // for dynamic mesh cases
    IOdictionary dynamicMeshDict
    (
        IOobject
        (
            "dynamicMeshDict",
            runTime.constant(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::NO_WRITE
        )
    );
    label refineInterval = 1;
    if (dynamicMeshDict.headerOk() && dynamicMeshDict.found("dynamicRefineFvMeshCoeffs"))
    {
        refineInterval = readScalar(dynamicMeshDict.subDict("dynamicRefineFvMeshCoeffs").lookup("refineInterval"));
    }


// add for post-processing of global Sherwood number 'on the fly'
    #include "getBubbleDiameter.H"

// get Henry coeffs:
    const PtrList<dimensionedScalar>& He = specTrans.pHe();

// get diffusion coefficients in liquid phase
    const PtrList<dimensionedScalar>& Dliq = specTrans.pD2();

// get average bubble concentration
    PtrList<dimensionedScalar> CiAve(n);
    PtrList<dimensionedScalar> CiAveOld(n);
    PtrList<dimensionedScalar> Sh(n);

    for (label i=0; i < C.size(); i++)
    {
        word fieldName = "CiAve" + Foam::name(i);
        CiAve.set
        (
            i,
            new dimensionedScalar
            (
                fieldName,
                dimensionSet(0,0,0,0,0),
                scalar(1.)
            )
        );

        fieldName = "CiAveOld" + Foam::name(i);
        CiAveOld.set
        (
            i,
            new dimensionedScalar
            (
                fieldName,
                dimensionSet(0,0,0,0,0),
                scalar(1.)
            )
        );

        fieldName = "Sh" + Foam::name(i);
        Sh.set
        (
            i,
            new dimensionedScalar
            (
                fieldName,
                dimensionSet(0,0,0,0,0),
                scalar(0.)
            )
        );
    }
    
    //PtrList<volScalarField> Shloc(n);
    PtrList<surfaceScalarField> Shloc(n);
    for (label i=0; i < C.size(); i++)
    {
        word fieldName = "Shloc" + Foam::name(i);
        Shloc.set
        (
            i,
            //new volScalarField
            new surfaceScalarField
            (
                IOobject
                (
                    fieldName,
                    runTime.timeName(),
                    mesh,
                    IOobject::NO_READ,
                    IOobject::NO_WRITE //AUTO_WRITE
                ),
                mesh,
                dimensionedScalar("Shloc", dimless, 0.)
            )
        );
    }

    // for post-processing: local Sherwood number calculation:
    //volVectorField fluxVec = fvc::reconstruct(Shloc[0]*mesh.magSf());
