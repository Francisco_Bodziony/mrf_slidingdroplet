//- read initial barycentre of bubble 1
vector barycentreAlpha1 = transportProperties.subDict("periodicBox").lookup("initBarycentre");

//- get cell label of cell containing barycentre-point
pRefCell = mesh.findCell(barycentreAlpha1);
