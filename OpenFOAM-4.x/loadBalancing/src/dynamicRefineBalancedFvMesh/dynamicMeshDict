/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.1.x                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      dynamicMeshDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

dynamicFvMesh   dynamicRefineBalancedFvMesh;

refinementControls
{
    enableRefinementControl  true;
    
    // to add:
    fields // must be scalars
    (
        alpha (min max refineLevel)
    )
    
    interface // must be a scalar
    (
        alpha
        {
            innerRefLayers 2;
            outerRefLayers 5;
            //optional settings
            maxRefineLevel 2; // default: assumed to be max refineLevel
            nAddLayers 3;    //default=0 ; specifies X:1 refinement at interface
        }
        beta
        {
            ...
        }
    )
    // END: to add
    
    gradients // must be scalars
    (
        T    (0.01 10 refineLevel)
        O2   (1.5   3 refineLevel)
        CH4  (1.5   3 refineLevel)
    );
    
    curls // must be vectors
    (
        U   (0.5 1 refineLevel)
    );
    
    regions
    (
        boxToCell
        {
            minLevel 1;
            
            box (-1 0.001 0.002)(1 0.005 0.003);
        }
    );
}

dynamicRefineFvMeshCoeffs
{
    // Extra entries for balancing
    enableBalancing true;
    allowableImbalance 0.15;

    // How often to refine
    refineInterval  15;
    
    // Field to be refinement on (set it to 'internalRefinementField' to use the
    // refinementControls dictionary entries above)
    field           internalRefinementField;
    
    // Refine field inbetween lower..upper
    lowerRefineLevel 0.5;
    upperRefineLevel maxRefinement+0.5;
    
    // If value < unrefineLevel unrefine
    unrefineLevel   -0.5;
    
    // Have slower than 2:1 refinement
    nBufferLayers   1;
    
    // Refine cells only up to maxRefinement levels
    maxRefinement   2;
    
    // Stop refinement if maxCells reached
    maxCells        200000;
    
    // Flux field and corresponding velocity field. Fluxes on changed
    // faces get recalculated by interpolating the velocity. Use 'none'
    // on surfaceScalarFields that do not need to be reinterpolated.
    correctFluxes
    (
        (phi Urel)
        (phiAbs U)
        (phiAbs_0 U_0)
        (nHatf none)
        (rho*phi none)
        (ghf none)
    );
    
    // Correct surfaceField mapping to get better initial phi-estimate
    // after mesh update
    mapSurfaceFields
    (
        Uf
        Uf_0
    );
    
    // Write the refinement level as a volScalarField
    dumpLevel       true;
}


// ************************************************************************* //
