/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 1991-2008 OpenCFD Ltd.
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Application
    testUtil

SourceFiles
    testUtil.C

Authors
    Daniel Deising     <deising@mma.tu-darmstadt.de>
    Daniel Rettenmaier <rettenmaier@gsc.tu-darmstadt.de>
    All rights reserved.

Description
    Executes mesh.update() (based on dynamicMeshDict settings)
    for dynamicRefineFvMesh-objects ONLY.
    Allows for reading in a list of scalar and vector fields as
    specified in the argument list.
    Tests surface field mapping of face center position vectors on any 
    given geometry.

    You may refer to this software as :
    //- full bibliographic data to be provided

    This code has been developed by :
        Daniel Deising (main developer).
    
    Method Development and Intellectual Property :
        Daniel Deising     <deising@mma.tu-darmstadt.de>
        Daniel Rettenmaier <rettenmaier@gsc.tu-darmstadt.de>
        Holger Marschall   <marschall@mma.tu-darmstadt.de>
        Dieter Bothe       <bothe@mma.tu-darmstadt.de>
        Cameron Tropea     <ctropea@sla.tu-darmstadt.de>

        Mathematical Modeling and Analysis
        Institute for Fluid Mechanics and Aerodynamics
        Center of Smart Interfaces
        Technische Universitaet Darmstadt
       
    If you use this software for your scientific work or your publications,
    please don't forget to acknowledge explicitly the use of it.

\*---------------------------------------------------------------------------*/

#include "fvCFD.H"
#include "dynamicFvMesh.H"
#include "dynamicRefineFvMesh.H"
#include <iostream>
#include <fstream>

using namespace Foam;

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    // add arguments to function call:
    timeSelector::addOptions(true, false);
#   include "addOverwriteOption.H"
#   include "addRegionOption.H"

    argList::addOption
    (
        "scalarFields",
        "scalarFields",
        "list of scalar fields needed for refinement"
    );
    argList::addOption
    (
        "vectorFields",
        "vectorFields",
        "list of vector fields needed for refinement"
    );

    #include "setRootCase.H"
    #include "createTime.H"
    #include "createDynamicFvMesh.H"
Info<< "created MESH" << endl;
    // read arguments if present
    List<word> scalarFields;
    List<word> vectorFields;
    
    if(args.optionFound("scalarFields"))
    {
        scalarFields = args.optionRead< List<word> >("scalarFields");
    }
    if(args.optionFound("vectorFields"))
    {
        vectorFields = args.optionRead< List<word> >("vectorFields");
    }

    PtrList<volScalarField> refineSFields(scalarFields.size());
    PtrList<volVectorField> refineVFields(vectorFields.size());

    forAll (scalarFields, i)
    {
        refineSFields.set
        (
            i,
            new volScalarField
            (
                IOobject
                (
                    scalarFields[i],
                    runTime.timeName(),
                    mesh,
                    IOobject::MUST_READ,
                    IOobject::AUTO_WRITE
                ),
                mesh
            )
        );
    }
    forAll (vectorFields, i)
    {
        refineVFields.set
        (
            i,
            new volVectorField
            (
                IOobject
                (
                    vectorFields[i],
                    runTime.timeName(),
                    mesh,
                    IOobject::MUST_READ,
                    IOobject::AUTO_WRITE
                ),
                mesh
            )
        );
    }

    
    IOdictionary refineDict
    (
        IOobject
        (
            "dynamicMeshDict",
            runTime.constant(),
            runTime,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    );
   
    surfaceVectorField Uf
    (
        IOobject
        (
            "Uf",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
        mesh,
        dimensionedVector("Uf", dimLength, vector::zero)
    );

    Uf=mesh.Cf();

    volVectorField U
    (
        IOobject
        (
            "U",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
        mesh,
        dimensionedVector("U", dimLength, vector::zero)
    );
    
    U = mesh.C();
    
    surfaceScalarField af
    (
        IOobject
        (
            "af",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
        mag(mesh.Cf())
    );
     
    scalar refineInterval = readScalar(refineDict.subDict("dynamicRefineFvMeshCoeffs").lookup("refineInterval"));
    
    // compute ddtCorr:
    surfaceScalarField ddtCorr1("ddtCorr1",fvc::ddtCorr(U, Uf));
    surfaceScalarField ddtCorr2("ddtCorr2",fvc::ddtCorr(U, Uf));
    
    // create oldTime fields:
    U.oldTime();
    Uf.oldTime();
    
    // get reference to oldTime-fields:
    const surfaceVectorField& Ufold = mesh.lookupObject<surfaceVectorField>("Uf_0");
    const volVectorField& Uold = mesh.lookupObject<volVectorField>("U_0");
    
    ddtCorr1.write();
    ddtCorr2.write();
    Ufold.write();
    Uold.write();

    runTime.writeNow();
    
    // get the current time to reset time after mesh update
    scalar time = runTime.time().value();
    for (int i=1; i <= refineInterval*5; i++)
    {
        runTime++;
        
        ddtCorr1 = fvc::ddtCorr(U, Uf);
        
        mesh.update();
        
        ddtCorr2 = fvc::ddtCorr(U, Uf);
        
        ddtCorr1.write();
        ddtCorr2.write();
        Ufold.write();
        Uold.write();
        
    Info<< " diff ddtCorr 1/2: " << max(mag(ddtCorr1-ddtCorr2)) << endl;
        runTime.writeNow();
    }

    if(args.optionFound("overwrite"))
    {
        runTime.setTime(time, 0);
    }

    runTime.writeAndEnd();

    Info<< "End\n" << endl;

    return(0);
}


// ************************************************************************* //
