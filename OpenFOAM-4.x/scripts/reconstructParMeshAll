#!/bin/bash
#
# Author: Daniel Rettenmaier
#
#- Does all the reconstruction steps
#- Spares already reconstructed steps
#- Deletes unfinished reconstructed time steps on abortion with ctrl+c
# 
#- Options: -latestTime, -time 0.01

# checks if a string is contained by a list
containsElement () {
  local e
  for e in "${@:2}"; do [[ "$e" == "$1" ]] && return 0; done
  return 1
}

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

# deletes incomplete last timestep
function ctrl_c() {
    echo ""
    echo "** Trapped CTRL-C"
    echo ""
  
    if [[ $f0 -eq 1 ]] &&  [[ $f3 -eq 0 ]] ; then
        rm -r ${line}
        echo "incomplete timestep deleted: $line"
    fi

    exit
}

#- script options
function options()
{
    if [[ "$1" == "-latestTime" ]]; then
        cd processor0
        latestTime=`ls -d [0-9]* | sort -g | tail -1`; 
        cd -

        echo "reconstructing: $latestTime"
        f0=1;f3=0;
        reconstructParMesh -latestTime >/dev/null 2>&1; 
        reconstructPar -latestTime >/dev/null 2>&1;
        reconstructParLevel -latestTime >/dev/null 2>&1;

        # for mrf simulations: check for restart.raw file
        if [ -f "./processor0/${latestTime}/restart.raw" ]
        then
            echo "restart.raw file exists!"
            cp ./processor0/${latestTime}/restart.raw ${latestTime}/restart.raw >/dev/null 2>&1;
        fi

        f3=1;f0=0;
        exit
    fi

    if [[ "$1" == "-time" ]]; then

        echo "reconstructing: $2"
        f0=1;f3=0;
        reconstructParMesh -time $2>/dev/null 2>&1; 
        reconstructPar -time $2>/dev/null 2>&1;
        reconstructParLevel -time $2>/dev/null 2>&1;

        # for mrf simulations: check for restart.raw file
        if [ -f "./processor0/$2/restart.raw" ]
        then
            echo "restart.raw file exists!"
            cp ./processor0/$2/restart.raw $2/restart.raw >/dev/null 2>&1;
        fi

        f3=1;f0=0;
        exit
    fi
}

#- reconstruct time single time steps
function reconstructTimeStep()
{
    timeStep=$1

    f0=1;f3=0;

    reconstructParMesh -time $timeStep >/dev/null 2>&1; 
    reconstructPar -time $timeStep >/dev/null 2>&1;
    reconstructParLevel -time $timeStep >/dev/null 2>&1;
    
    # for mrf simulations: check for restart.raw file
    if [ -f "./processor0/${timeStep}/restart.raw" ]
    then
        echo "restart.raw file exists!"
        cp ./processor0/${timeStep}/restart.raw ${timeStep}/restart.raw >/dev/null 2>&1;
    fi

    f3=1;f0=0;
}

#- checks if and which time steps are left for reconstruction
function createTimeListToReconstruct()
{
    echo "Create new List of time steps to reconstruct"
    # find decomposed timesteps
    cd processor0

    # get all folders which name begins with a number and sort them
    decompTimeList=`ls -d [0-9]* | sort -g`;
    cd -   >/dev/null 2>&1;

    # find reconstructed timesteps
    timeList=`ls -d [0-9]* | sort -g`
    timeList=($timeList)

    #- check if and which time steps are left for reconstruction
    reconstrList=(); timesLeft=false;
    for line in $decompTimeList
    do
        containsElement "$line" "${timeList[@]}"
        reconstruct=$?
        if [ 0 != $reconstruct ]; then
            reconstrList+=("$line");
            timesLeft=true;
        fi
    done
}


#############################################################

options $@

SECONDS=0

#- create new List of time steps
createTimeListToReconstruct

#- reconstruct time steps and check if new steps are created

while $timesLeft   
do
    count=0;
    for timeStep in ${reconstrList[@]}
    do
        count=$(expr $count + 1);
        echo -e "Reconstruct: $timeStep   \t$count/${#reconstrList[@]}"
        reconstructTimeStep $timeStep
    done

    sleep 1.5

    #- create new List of time steps
    createTimeListToReconstruct
done

echo "reconstruction finished in "$(($SECONDS / 60)) min and $(($SECONDS % 60)) s .""

# remove temp file
if [ -f logTmp ] ; then
    rm logTmp;
fi
