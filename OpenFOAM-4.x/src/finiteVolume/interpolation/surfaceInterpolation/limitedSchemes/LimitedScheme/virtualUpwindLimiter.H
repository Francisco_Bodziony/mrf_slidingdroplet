/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011-2012 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::virtualUpwindLimiter

Authors
    Tomislav Maric     <maric@mma.tu-darmstadt.de>
    Daniel Deising     <deising@mma.tu-darmstadt.de>
    All rights reserved.

Description
    Virtual upwind limiting for scalar-type interpolation schemes based
    on local vicinity search algorithm.

    You may refer to this software as :
    //- ADD PAPER REFERENCE

    This code has been developed by :
        Daniel Deising (main developer).
    
    Method Development and Intellectual Property :
        Tomislav Maric   <maric@mma.tu-darmstadt.de>
        Daniel Deising   <deising@mma.tu-darmstadt.de>
        Holger Marschall <marschall@mma.tu-darmstadt.de>
        Dieter Bothe     <bothe@mma.tu-darmstadt.de>

        Mathematical Modeling and Analysis
        Center of Smart Interfaces
        Technische Universitaet Darmstadt
       
    If you use this software for your scientific work or your publications,
    please don't forget to acknowledge explicitly the use of it.

\*---------------------------------------------------------------------------*/

#ifndef virtualUpwindLimiter_H
#define virtualUpwindLimiter_H


#include <set>

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                           Class LimitedScheme Declaration
\*---------------------------------------------------------------------------*/

class virtualUpwindLimiter
{
private:

    // Private Data member
        mutable scalar lastDistance_;


public:

    //typedef scalar phiType;
    //typedef vector gradPhiType;

    // Null Constructor
        virtualUpwindLimiter()
        :
            lastDistance_(0.0)
        {}


    // Member Functions

        //- Check is point p is in cell I
        bool pointIsInCell
        (
            const point p,
            const label cellLabel,
            const fvMesh& mesh,
            scalar tolerance=SMALL
        ) const
        {
            const cellList& cells = mesh.cells(); 
            const cell& cell = cells[cellLabel];
            const labelList& own = mesh.faceOwner(); 
            const vectorField& Cf = mesh.faceCentres(); 
            const vectorField& Sf = mesh.faceAreas(); 

            bool pointIsInside = true;

            // For all face labels of the cell.
            forAll (cell, I) 
            {
                label faceLabel = cell[I];
                vector faceNormal = Sf[faceLabel];

                // If the cell does not own the face.
                if (! (cellLabel == own[cell[I]])) 
                {
                    faceNormal *= -1;  
                }

                // Compute the vector from the face center to the point p.
                vector fp = p - Cf[cell[I]];  

                if ((fp & faceNormal) > tolerance) 
                {
                    //Info << "point outside face = " << (fp & faceNormal) << endl; 
                    pointIsInside = false;
                    break;
                }
                //else
                //{
                //Info << "point inside face = " << (fp & faceNormal) << endl; 
                //}
            }

            return pointIsInside;
        }


        labelList pointCellStencil
        (
            const label cellLabel,
            const fvMesh& mesh
        ) const
        {
            const faceList& faces = mesh.faces(); 
            const cellList& cells = mesh.cells(); 
            const labelListList& pointCells = mesh.pointCells(); 

            labelList cellPoints = cells[cellLabel].labels(faces); 

            std::set<label> newNeighborCells;

            forAll (cellPoints, I)
            {
                const labelList& addedNeighborCells = pointCells[cellPoints[I]]; 
                forAll(addedNeighborCells, J)
                {
                    newNeighborCells.insert(addedNeighborCells[J]);
                }
            }
            
            labelList stencil(newNeighborCells.size());
            forAll(stencil, idx)
            {
                stencil[idx] = *std::next(newNeighborCells.begin(), idx);
            }

            //return labelList(newNeighborCells.begin(), newNeighborCells.end());  
            return stencil;
        }


        //- Get cell label containing point p
        label cellContainingPoint
        (
            const point& p,
            const fvMesh& mesh,
            const label seedCell
        )const
        {
            if (pointIsInCell(p, seedCell, mesh))
            {
                return seedCell;  
            }

            const volVectorField& C = mesh.C(); 
            scalar minDistance = mag(C[seedCell] - p);
            label minDistanceCell = seedCell;

            // For all neighbour cells of the seed cell. 
            const labelListList& cellCells = mesh.cellCells(); 
            labelList neighborCells = cellCells[seedCell]; 

            const cellList& cells = mesh.cells(); 

            // Extend the tetrahedral stencil.
            if (cells[seedCell].size() == 4)
            {
                neighborCells = pointCellStencil(seedCell, mesh);  
            }

            //Info << "used stencil = " << neighborCells  << endl;
            forAll (neighborCells, I) 
            {
                label neighborCell = neighborCells[I]; 

                if (pointIsInCell(p, neighborCell, mesh)) 
                {
                    lastDistance_ = minDistance; 
                    return neighborCell; 
                }

                scalar distance = mag(C[neighborCell] - p); 

                // Set label of the cell with the minimal distance. 
                if (distance <= minDistance) 
                {
                    minDistance = distance; 
                    minDistanceCell = neighborCell; 
                }
            }

            if (pointIsInCell(p, minDistanceCell, mesh)) 
            {
                lastDistance_ = minDistance; 
                return minDistanceCell; 
            }
            else 
            {
                if (mag(lastDistance_ - minDistance) < SMALL)
                {
                    return minDistanceCell; 
                }
                else
                {
                    lastDistance_ = minDistance; 
                    // Recursion
                    return cellContainingPoint(p, mesh, minDistanceCell); 
                }
            }

            return -1; 
        }


        //- Limit the virtual upwind node based on the values in neighbouring cells
        scalar limitVU
        (
            const volScalarField& phi, // field to be interpolated; NOT flux field!!
            const point& p,
            const label faceI,
            const scalar faceFlux,
            scalar phiVU
        ) const
        {
            const fvMesh& mesh = phi.mesh();

            // get cell ID of centre cell (U->C->D)
            label own = mesh.faceOwner()[faceI];
            label nei = mesh.faceNeighbour()[faceI];

            label seedCell = nei;

            if (faceFlux > 0)
            {
                seedCell = own;
            }

            // get nearest cell to virtual upwind point p (Algorithm: Tomislav Maric)
            label nearestCell = cellContainingPoint(p, mesh, seedCell);

            // get all neighbour cells of cellID and limit virtual upwind with min/max
            labelList cellNeighbours = mesh.cellCells()[nearestCell];

            List<scalar> phiCells;

            forAll(cellNeighbours, I)
            {
                phiCells.append(phi[cellNeighbours[I]]); // should be phi (field to be interpolated) instead vsf_ (alphaField)
            }

            scalar minPhiCells = min(phiCells);
            scalar maxPhiCells = max(phiCells);

            // copy phiVU to compare with new one
            scalar phiVUcp = phiVU;

            // limit virtual upwind value
            phiVU = max(minPhiCells, min(phiVU, maxPhiCells));

            if (surfaceInterpolation::debug)
            {
                if (phiVUcp != phiVU)
                {
                    Info<< " limit virtual upwind node in cell: " << nearestCell << endl;
                    //isLimited_[nearestCell] = 1;

                    // check algorithm (works for uniform carthesian grid ONLY!):
                    point c = mesh.C()[nearestCell];
                    if (mag(c-p) > 1e-07)
                    {
                        Info<< "If carthesian grid: search algorithm found wrong cell !!! " << endl;
                    }
                }
            }
    
            return phiVU;
        };
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
