/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    Foam::MHRICblended

SourceFiles
    MHRICblended.C

Authors
    Hrvoje Jasak       Wikki Ltd.
    Daniel Deising     <deising@mma.tu-darmstadt.de>
    All rights reserved.

Description
    MHRIC blended differencing scheme. Uses MHRIC at interface and high
    resolution base scheme elsewhere.

    You may refer to this software as :
    //- Deising et. al. 2016, Chem. Eng. Sci. 139, pp. 173-195

    This code has been developed by :
        Daniel Deising (main developer).
    
    Method Development and Intellectual Property :
        Daniel Deising   <deising@mma.tu-darmstadt.de>
        Holger Marschall <marschall@mma.tu-darmstadt.de>
        Dieter Bothe     <bothe@mma.tu-darmstadt.de>

        Mathematical Modeling and Analysis
        Center of Smart Interfaces
        Technische Universitaet Darmstadt
       
    If you use this software for your scientific work or your publications,
    please don't forget to acknowledge explicitly the use of it.

\*---------------------------------------------------------------------------*/

#ifndef MHRICblended_H
#define MHRICblended_H

#include "limitedSurfaceInterpolationScheme.H"
#include "interfaceCapturingScheme.H"
#include "fvc.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                           Class MHRICblended Declaration
\*---------------------------------------------------------------------------*/

class MHRICblended
:
    public interfaceCapturingScheme
{
    // Private data

        //- all private data already declared in base class 'interfaceCapturingScheme'

    // Private Member Functions

        //- Disallow default bitwise copy construct
        MHRICblended(const MHRICblended&);

        //- Disallow default bitwise assignment
        void operator=(const MHRICblended&);


        //- Calculate individual weight
        scalar weight
        (
            const volScalarField& phi,
            const scalar cdWeight,
            const scalar faceFlux,
            const scalar& phiP,
            const scalar& phiN,
            const vector& gradcP,
            const vector& gradcN,
            const scalar Cof,
            const vector d,
            const point cP,
            const point cN,
            const label faceI,
            const scalar blendingFactor
        ) const;

public:

    //- Runtime type information
    TypeName("MHRICblended");


    // Constructors

        //- Construct from mesh, faceFlux and blendingFactor
        MHRICblended
        (
            const fvMesh& mesh,
            const surfaceScalarField& faceFlux//,
            //const volScalarField& vsf
        )
        :
            //interfaceCapturingScheme(mesh, faceFlux, vsf)
            interfaceCapturingScheme(mesh, faceFlux)
        {}

        //- Construct from mesh and Istream.
        //  The name of the flux field is read from the Istream and looked-up
        //  from the mesh objectRegistry
        MHRICblended
        (
            const fvMesh& mesh,
            Istream& is
        )
        :
            interfaceCapturingScheme(mesh, is)
        {}

        //- Construct from mesh, faceFlux and Istream
        MHRICblended
        (
            const fvMesh& mesh,
            const surfaceScalarField& faceFlux,
            Istream& is
        )
        :
            interfaceCapturingScheme(mesh, faceFlux, is)
        {}


    // Member Functions

        virtual void calculateBlendingFactor() const;
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
