/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | foam-extend: Open Source CFD
   \\    /   O peration     |
    \\  /    A nd           | For copyright notice see file Copyright
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of foam-extend.

    foam-extend is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    foam-extend is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with foam-extend.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "EulerImplicit.H"
#include "chemistryModel.H"
#include "addToRunTimeSelectionTable.H"
#include "simpleMatrix.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

defineTypeNameAndDebug(EulerImplicit, 0);

addToRunTimeSelectionTable(chemistrySolver, EulerImplicit, dictionary);

}

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::EulerImplicit::EulerImplicit
(
    chemistryModel& model,
    const word& modelName
)
:
    chemistrySolver(model, modelName),
    coeffsDict_(model.subDict(modelName + "Coeffs")),
    cTauChem_(readScalar(coeffsDict_.lookup("cTauChem"))),
    equil_(coeffsDict_.lookup("equilibriumRateLimiter"))
{}

// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::EulerImplicit::~EulerImplicit()
{}

// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

Foam::scalar Foam::EulerImplicit::solve
(
    scalarField &c,
    const scalar t0,
    const scalar dt
) const
{
    scalar pf, cf, pr, cr;
    label lRef, rRef;

    label nSpecie = this->model_.nSpecie();
    simpleMatrix<scalar> RR(nSpecie);

    for (label i=0; i<nSpecie; i++)
    {
        c[i] = max(0.0, c[i]);
    }

    for (label i=0; i<nSpecie; i++)
    {
        RR.source()[i] = c[i]/dt;
    }

    for (label i=0; i<this->model_.reactions().size(); i++)
    {
        const Reaction& R = this->model_.reactions()[i];

        scalar omegai = this->model_.omega
        (
            R, c, pf, cf, lRef, pr, cr, rRef
        );

        scalar corr = 1.0;
        if (equil_)
        {
            if (omegai<0.0)
            {
                corr = 1.0/(1.0 + pr*dt);
            }
            else
            {
                corr = 1.0/(1.0 + pf*dt);
            }
        }

        for (label s=0; s<R.lhs().size(); s++)
        {
            label si = R.lhs()[s].index;
            scalar sl = R.lhs()[s].stoichCoeff;
            RR[si][rRef] -= sl*pr*corr;
            RR[si][lRef] += sl*pf*corr;
        }

        for (label s=0; s<R.rhs().size(); s++)
        {
            label si = R.rhs()[s].index;
            scalar sr = R.rhs()[s].stoichCoeff;
            RR[si][lRef] -= sr*pf*corr;
            RR[si][rRef] += sr*pr*corr;
        }

    } // end for(label i...


    for (label i=0; i<nSpecie; i++)
    {
        RR[i][i] += 1.0/dt;
    }

    c = RR.LUsolve();
    for (label i=0; i<nSpecie; i++)
    {
        c[i] = max(0.0, c[i]);
    }

    // estimate the next time step
    scalar tMin = GREAT;
    label nEqns = this->model_.nEqns();
    scalarField c1(nEqns, 0.0);

    for (label i=0; i<nSpecie; i++)
    {
        c1[i] = c[i];
    }

    scalarField dcdt(nEqns, 0.0);
    this->model_.derivatives(0.0, c1, dcdt);

    scalar sumC = sum(c);

    for (label i=0; i<nSpecie; i++)
    {
        scalar d = dcdt[i];
        if (d < -SMALL)
        {
            tMin = min(tMin, -(c[i] + SMALL)/d);
        }
        else
        {
            d = max(d, SMALL);
            scalar cm = max(sumC - c[i], 1.0e-5);
            tMin = min(tMin, cm/d);
        }
    }

    return cTauChem_*tMin;
}

//Foam::label Foam::EulerImplicit::nEqns() const
//{
//    // nEqns = number of species + temperature + pressure
//    //return nSpecie_ + 2;
//    return this->model_.nSpecie() + 2;
//}

// ************************************************************************* //
