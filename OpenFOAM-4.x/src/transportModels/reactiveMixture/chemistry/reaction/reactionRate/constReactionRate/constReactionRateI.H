/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | foam-extend: Open Source CFD
   \\    /   O peration     |
    \\  /    A nd           | For copyright notice see file Copyright
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of foam-extend.

    foam-extend is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    foam-extend is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with foam-extend.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

// Construct from components
inline constReactionRate::constReactionRate
(
    const scalar kf,
    const scalar kr
)
:
    kf_(kf),
    kr_(kr)
{}


//- Construct from Istream
inline constReactionRate::constReactionRate
(
    const speciesTable&,
    Istream& is
)
:
    kf_(readScalar(is.readBegin("constReactionRate(Istream&)"))),
    kr_(readScalar(is))
{
    is.readEnd("constReactionRate(Istream&)");
}


//- Construct from dictionary
inline Foam::constReactionRate::constReactionRate
(
    const speciesTable&,
    const dictionary& dict
)
:
    kf_(readScalar(dict.lookup("kf"))),
    kr_(readScalar(dict.lookup("kr")))
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

inline scalar constReactionRate::operator()
(
    const scalar T,
    const scalar,
    const scalarField&
) const
{
    scalar k(0.0);

    if (T >= 0.0)
    {
        k = kf_;
    }
    else
    {
        k = kr_;
    }

    return k;
}


inline void constReactionRate::write(Ostream& os) const
{
    os.writeKeyword("kf") << kf_ << token::END_STATEMENT << nl;
    os.writeKeyword("kr") << kr_ << token::END_STATEMENT << nl;
}

inline Ostream& operator<<(Ostream& os, const constReactionRate& cst)
{
    os  << token::BEGIN_LIST
        << cst.kf_ << token::SPACE << cst.kr_
        << token::END_LIST;
    return os;
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// ************************************************************************* //
