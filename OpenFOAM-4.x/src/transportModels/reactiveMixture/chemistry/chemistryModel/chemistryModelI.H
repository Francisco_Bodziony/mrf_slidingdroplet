/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 1991-2008 OpenCFD Ltd.
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

\*---------------------------------------------------------------------------*/

#include "zeroGradientFvPatchFields.H"

// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

inline Foam::PtrList<Foam::scalarField>&
Foam::chemistryModel::RR()
{
    return RR_;
}

inline const Foam::PtrList<Foam::volScalarField>&
Foam::chemistryModel::C() const
{
    return C_;
}

inline const Foam::PtrList<Foam::Reaction>&
Foam::chemistryModel::reactions() const
{
    return reactions_;
}

inline Foam::label
Foam::chemistryModel::nSpecie() const
{
    return nSpecie_;
}

inline Foam::label
Foam::chemistryModel::nReaction() const
{
    return nReaction_;
}

inline const Foam::chemistrySolver&
Foam::chemistryModel::solver() const
{
    return solver_;
}

inline Foam::tmp<Foam::volScalarField> Foam::chemistryModel::RR
(
    const label i
) const
{
    tmp<volScalarField> tRR
    (
        new volScalarField
        (
            IOobject
            (
                "RR(" + C_[i].name() + ")",
                this->time().timeName(),
                mesh_,
                IOobject::NO_READ,
                IOobject::NO_WRITE
            ),
            mesh_,
            dimensionedScalar("zero", dimensionSet(0, -3, -1, 0, 1), 0.0),
            zeroGradientFvPatchScalarField::typeName
        )
    );

    if (chemistry_)
    {
        tRR.ref().primitiveFieldRef() = RR_[i];
        tRR.ref().correctBoundaryConditions();
    }

    return tRR;
}

inline const Foam::fvMesh& Foam::chemistryModel::mesh() const
{
    return mesh_;
}


inline Foam::Switch Foam::chemistryModel::chemistry() const
{
    return chemistry_;
}


inline const Foam::scalarField& Foam::chemistryModel::deltaTChem() const
{
    return deltaTChem_;
}


inline Foam::scalarField& Foam::chemistryModel::deltaTChem()
{
    return deltaTChem_;
}

// ************************************************************************* //
