/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Version:  2.4.x                               
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    Foam::alphaInterpolation::inverseDistanceAlphaInterpolation

SourceFiles
    inverseDistanceAlphaInterpolation.C

Authors 
    Christian Kunkelmann (formerly TTD, TU Darmstadt)
    Stefan Batzdorf (formerly TTD, TU Darmstadt)
    Daniel Rettenmaier <rettenmaier@gsc.tu-darmstadt.de>

Description
    Interpolate alpha from cells to points by inverseDistancing for usage
    in interfaceReconstruction

    You may refer to this software as :
    //- full bibliographic data to be provided

    This code has been developed by :
        Daniel Rettenmaier <rettenmaier@gsc.tu-darmstadt.de> (main developer).
    
    Method Development and Intellectual Property :
        Daniel Rettenmaier <rettenmaier@gsc.tu-darmstadt.de>
        Daniel Deising     <deising@mma.tu-darmstadt.de>
        Holger Marschall   <marschall@mma.tu-darmstadt.de>
        Dieter Bothe       <bothe@mma.tu-darmstadt.de>
        Cameron Tropea     <ctropea@sla.tu-darmstadt.de>

        Mathematical Modeling and Analysis
        Institute for Fluid Mechanics and Aerodynamics
        Center of Smart Interfaces
        Technische Universitaet Darmstadt
       
    If you use this software for your scientific work or your publications,
    please don't forget to acknowledge explicitly the use of it.

\*---------------------------------------------------------------------------*/


#ifndef inverseDistanceAlphaInterpolation_H
#define inverseDistanceAlphaInterpolation_H

#include "alphaInterpolation.H"
#include "syncTools.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
/*---------------------------------------------------------------------------*\
                Class inverseDistanceAlphaInterpolation Declaration
\*---------------------------------------------------------------------------*/

class inverseDistanceAlphaInterpolation
:
	public alphaInterpolation
{

    // Private data

        //- weights for interpolation from cells to points
        scalarListList cellPointWeights_;

        //- weights for interpolation from boundary faces to points
        scalarListList boundaryFacePointWeights_;

        //- time index makeWeights had been called to last time
        //  prevents makeWeights from being called more than once within a single time step
        label lastUpdateTime_;

public:
    //- Runtime type information
    TypeName("inverseDistance");

    // Constructors

        //- Construct from components
        inverseDistanceAlphaInterpolation
        (
            const word& name,
            const volScalarField& alpha,
            const List<bool>& isWallPatch
        );

    // Destructor

        ~inverseDistanceAlphaInterpolation();

    // Member Functions
	
        //- make weights for the interpolation
        void makeWeights();

        //- interpolate
        void interpolate();

        //- interpolate only for points on walls, i.e. contact line patches
        void updateAlphaPWall();
};

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
