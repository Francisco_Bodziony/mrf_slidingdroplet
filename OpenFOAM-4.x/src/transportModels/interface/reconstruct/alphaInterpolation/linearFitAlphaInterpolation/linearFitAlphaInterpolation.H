/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Version:  2.4.x                               
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    Foam::alphaInterpolation::linearFitAlphaInterpolation

SourceFiles
    linearFitAlphaInterpolation.C

Authors
    Stefan Batzdorf (formerly TTD, TU Darmstadt)
    All rights reserved.

Description
    
    You may refer to this software as :
    //- full bibliographic data to be provided

    This code has been developed by :
        Stefan Batzdorf (formerly TTD, TU Darmstadt)
    
    Method Development and Intellectual Property :
        Stefan Batzdorf    (formerly TTD, TU Darmstadt)
        Daniel Rettenmaier <rettenmaier@gsc.tu-darmstadt.de>
        Daniel Deising     <deising@mma.tu-darmstadt.de>
        Holger Marschall   <marschall@mma.tu-darmstadt.de>
        Dieter Bothe       <bothe@mma.tu-darmstadt.de>
        Cameron Tropea     <ctropea@sla.tu-darmstadt.de>

        Mathematical Modeling and Analysis
        Institute for Fluid Mechanics and Aerodynamics
        Center of Smart Interfaces
        Technische Universitaet Darmstadt
       
    If you use this software for your scientific work or your publications,
    please don't forget to acknowledge explicitly the use of it.

\*---------------------------------------------------------------------------*/


#ifndef linearFitAlphaInterpolation_H
#define linearFitAlphaInterpolation_H

#include "alphaInterpolation.H"
#include "scalarMatrices.H"
#include "coordinateSystem.H"
#include "syncTools.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
/*---------------------------------------------------------------------------*\
                Class linearFitAlphaInterpolation Declaration
\*---------------------------------------------------------------------------*/

class linearFitAlphaInterpolation
:
	public alphaInterpolation
{
    // Privat class

        //- new option for syncTools, combines dynamic lists
        //  used to combine cell coordinates and values from different processors
        //  no need to check for duplicates as cells cannot be on different
        //  processors at the same time
        template <class T>
        class appendOp
        {
        public:
            void operator()(DynamicList<T>& x, const DynamicList<T>& y) const
            {
                forAll(y, i)
                {
                    x.append(y[i]);
                }
            }
        };

    // Private data

        //- geometric order of surface wich can be fitted at a point
        //  0: all neighbour points are on a straight
        //  1: all neighbour points are on a plane
        //  2: 3-dimensional case
		labelList surfaceOrder_;

        //- vector in the plane if surfaceOrder = 1
        vectorField planeDirs_;

        //- vector normal to the plane if surfaceOrder = 1
        vectorField planeAxes_;

        //- centres of cells attached to a point
        List<DynamicList<vector> > attachedCellPositions_;

        //- cell-to-point weights for fitting
        List<DynamicList<scalar> > cellWeights_;

        //- centres of boundary faces attached to a point
        List<DynamicList<vector> > attachedFacePositions_;

        //- face-to-point weights for fitting
        List<DynamicList<scalar> > faceWeights_;

        //- centres of wall faces attached to a point
        List<DynamicList<vector> > attachedWallFacePositions_;

        //- wall face-to-point weights for fitting
        List<DynamicList<scalar> > wallFaceWeights_;

        //- time index makeWeights had been called to last time
        //  prevents makeWeights from being called more than once within a single time step
        label lastUpdateTime_;

public:
    //- Runtime type information
    TypeName("linearFit");

    // Constructors

        //- Construct from components
        linearFitAlphaInterpolation
        (
            const word& name,
            const volScalarField& alpha,
            const List<bool>& isWallPatch
        );

    // Destructor

        ~linearFitAlphaInterpolation();

    // Member Functions
	
        //- make weights for the interpolation
        void makeWeights();

        //- interpolate
        void interpolate();

        //- interpolate only for points on contactAngle patches
        void updateAlphaPWall();
};

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
