/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Version:  2.4.x                               
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    Foam::alphaInterpolation

SourceFiles
    alphaInterpolation.C

Authors:
    Christian Kunkelmann (formerly TTD, TU Darmstadt)
    Stefan Batzdorf      (formerly TTD, TU Darmstadt)
    Daniel Rettenmaier   <rettenmaier@gsc.tu-darmstadt.de>

Description
    Basic class for interpolation methods of alpha from cells to points for
    usage in interfaceReconstruction

    You may refer to this software as :
    //- full bibliographic data to be provided

    This code has been developed by :
        Stefan Batzdorf (main developer).
    
    Method Development and Intellectual Property :
        Stefan Batzdorf    (formerly TTD, TU Darmstadt)
        Daniel Rettenmaier <rettenmaier@gsc.tu-darmstadt.de>
        Daniel Deising     <deising@mma.tu-darmstadt.de>
        Holger Marschall   <marschall@mma.tu-darmstadt.de>
        Dieter Bothe       <bothe@mma.tu-darmstadt.de>
        Cameron Tropea     <ctropea@sla.tu-darmstadt.de>

        Mathematical Modeling and Analysis
        Institute for Fluid Mechanics and Aerodynamics
        Center of Smart Interfaces
        Technische Universitaet Darmstadt
       
    If you use this software for your scientific work or your publications,
    please don't forget to acknowledge explicitly the use of it.

\*---------------------------------------------------------------------------*/


#ifndef alphaInterpolation_H
#define alphaInterpolation_H

#include "IOdictionary.H"
#include "volFields.H"
#include "fvCFD.H"

#include "typeInfo.H"
#include "runTimeSelectionTables.H"
#include "autoPtr.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                Class alphaInterpolation Declaration
\*---------------------------------------------------------------------------*/

class alphaInterpolation

{

protected:
    // Protected data
        word name_;
    	const Time& runTime_;
	    const fvMesh& mesh_;
		const volScalarField& alpha_;

        //- interpolated values of alpha at the mesh points
        scalarField alphaP_;

        const List<bool>& isWallPatch_;

private:
    // Private Member Functions

        //- Disallow copy construct
        alphaInterpolation(const alphaInterpolation&);

        //- Disallow default bitwise assignment
        void operator=(const alphaInterpolation&);

public:
    //- Runtime type information
    TypeName("alphaInterpolation");

    // Declare run-time constructor selection table

        declareRunTimeSelectionTable
        (
            autoPtr,
            alphaInterpolation,
            dictionary,
            (
                const word& name,
                const volScalarField& alpha,
                const List<bool>& isWallPatch
            ),
            (name, alpha, isWallPatch)
        );
    // Selectors

        //- Return a reference to the selected alphaInterplationModel
        static autoPtr<alphaInterpolation> New
        (
            const word& name,
            const volScalarField& alpha,
            const List<bool>& isWallPatch
        );

	// Constructors

        //- Construct from components
        alphaInterpolation
        (
            const word& name,
            const volScalarField& alpha,
            const List<bool>& isWallPatch
        );

    // Destructor

        virtual ~alphaInterpolation();

    // Member Functions

		//- Access
        word name() const
        {
            return name_;
        }

        //- Return the implicit part of the source term for the energy equation
        const scalarField& alphaP() const
        {
            return alphaP_;
        }

        //- make weights for the interpolation
		virtual void makeWeights() = 0;

        //- interpolate
        virtual void interpolate() = 0;

        //- interpolate only for points on contactAngle patches
        virtual void updateAlphaPWall(){};

        //- correct the values on wall boundaries
        void correctWallFaces(const surfaceScalarField&);

};

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
