/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "reconstruct.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(reconstruct, 0);
    defineRunTimeSelectionTable(reconstruct, dictionary);
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //


Foam::reconstruct::reconstruct
(
    const word& name,
    const volScalarField& alpha,
    const dictionary& transpProp, 
    const List<bool>& isWallPatch,
    const volScalarField& isInterface
)
:
    name_(name),
    alpha1_(alpha),

    nHatv_
    (
        IOobject
        (
            "nHat" + Foam::word(alpha.name()),
            alpha.mesh().time().timeName(),
            alpha.mesh(),
            IOobject::READ_IF_PRESENT,
            IOobject::NO_WRITE //AUTO_WRITE
        ),
        alpha.mesh(),
        dimensionedVector("nHat", dimensionSet(0,0,0,0,0,0,0), vector (0, 0, 0))
    ),

    nHatfv_
    (
        IOobject
        (
            "nHatfvRecon" + Foam::word(alpha.name()),
            alpha.mesh().time().timeName(),
            alpha.mesh(),
            IOobject::READ_IF_PRESENT,
            IOobject::NO_WRITE
        ),
        alpha.mesh(),
        dimensionedVector("nHatfv", dimensionSet(0,0,0,0,0,0,0), vector(0,0,0))
    ),

    nHatf_
    (
        IOobject
        (
            "nHatfRecon" + Foam::word(alpha.name()),
            alpha.time().timeName(),
            alpha.mesh()
        ),
        alpha.mesh(),
        dimensionedScalar("nHatf", dimArea, 0.0)
    ),   

    interfaceDensity_
    (
        IOobject
        (
            "interfaceDensity" + Foam::word(alpha.name()),
            alpha.mesh().time().timeName(),
            alpha.mesh(),
            IOobject::NO_READ,
            IOobject::NO_WRITE //AUTO_WRITE
        ),
        alpha.mesh(),
        dimensionedScalar("interfaceDensity", dimensionSet(0,-1,0,0,0,0,0), 0.0)
    ),

    isInterface_(isInterface),

    isWallPatch_(isWallPatch),

    deltaN_
    (
        "deltaN",
        1e-8/pow(average(alpha1_.mesh().V()), 1.0/3.0)
    )



{
    Info << "       reconstruct::reconstruct()" << endl;

}


// ************************************************************************* //
