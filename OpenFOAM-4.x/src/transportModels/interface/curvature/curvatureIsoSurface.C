/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "curvatureIsoSurface.H"
#include "addToRunTimeSelectionTable.H"

namespace Foam
{


defineTypeNameAndDebug( curvatureIsoSurface, 0 );
addToRunTimeSelectionTable( curvature, curvatureIsoSurface, dictionary );

/*
//- finds all boundary cells which contain interface but have no contact line
boolList findThinFilmCells()
{
    //- Find thin film cells, criteria are: interfaceDensity>0, no cl , all neighbour
    //  cells at the wall interfaceDensity>0
    boolList boundaryCell(mesh_.nCells(), false);
    boolList boundaryInterfaceDensity(mesh_.nCells(), false);
    boolList candidate(mesh_.nCells(), false);
    boolList thinFilmCell(mesh_.nCells(), false);
    if (thinFilm_)
    {
        forAll(alpha1_.boundaryField(), patchI)
        {
            if (isWallPatch_(alpha1_.boundaryField()[patchI]))
            {
                forAll(mesh_.boundaryMesh()[patchI], faceI)
                {
                    label faceIGlobal = faceI + mesh_.boundaryMesh()[patchI].start();
                    label own = mesh_.faceOwner()[faceIGlobal];
                    boundaryCell[own] = true;
                    if (interfaceDensity_[own]>0)
                    {
                        boundaryInterfaceDensity[own]=true;
                        if (  //if no cLine cell)
                        {
                            candidate[own]=true;
                        }
                    }
                }
                forAll(mesh_.boundaryMesh()[patchI], faceI)
                {
                    label faceIGlobal = faceI + mesh_.boundaryMesh()[patchI].start();
                    label own = mesh_.faceOwner()[faceIGlobal];
                    if (candidate[own])
                    {
                        thinFilmCell[own]=true;     // Aussume thinFilmCell until opposite is proved
                        forAll(mesh_.cellCells()[own],cellI)
                        {
                            label nei=mesh_.cellCells()[own][cellI];
                            if (boundaryCell[nei])
                            {
                                if(!boundaryInterfaceDensity[nei])
                                {
                                    thinFilmCell[own]=false;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return thinFilmCell;
}

*/


/*
    Calculates a modified version of the nHat field, in which nHat is deleted in
    cells containing contact line, since the interface is not correct here.
    The curvature field in the deleted cells is then set by taking the average
    with neighbour cells. //TODO Thereby thin film cells are spared.
*/
void curvatureIsoSurface::prepareNHatCurv()
{
    //- only keeps nHatv_ of interface cells
    dimensionedScalar sMall("sMall", dimensionSet(0, -1, 0, 0, 0, 0, 0), SMALL);
    nHatCurvature_ = nHatv_ * pos(interfaceDensity_ - SMALL);

    // boolList thinFilmCell = findThinFilmCells();                        //TODO

    //- delete nHatCurvature in the cells close to the contact line,
    //  is needed because reconstruction does not work properly there
    //  because the interpolation of alpha1_ to the points does not take
    //  into account face values at these boundaries (which leads to wrong nHatv)

    boolList nHatDeleted (mesh_.nCells(), false);
    bool deletedCellsLeft = false;

    forAll(nHatCurvature_.boundaryField(), iPatch)
    {
        if (isWallPatch_[iPatch])
        {
            forAll(nHatCurvature_.boundaryField()[iPatch], iFace)
            {
                label iFaceGlobal = iFace + mesh_.boundaryMesh()[iPatch].start();
                label own = mesh_.faceOwner()[iFaceGlobal];
//orig                if (interfaceDensity_[own] != 0.0 && !thinFilmCell[own])
                if (interfaceDensity_[own] != 0.0 )
                    //if (contactLinePoints_[iPatch][iFace].size() == 2)
                {
                    nHatCurvature_[own] = vector (0, 0, 0);
                    nHatDeleted[own] = true;
                    deletedCellsLeft = true;
                }
            }
        }
    }

    nHatCurvature_.correctBoundaryConditions();

    //- reset nHatCurvature_ where it has been deleted by taking the average
    //  with neighbour cells
    int iLoop = 0;
    reduce(deletedCellsLeft, orOp<bool>());
    while ((deletedCellsLeft) && (iLoop < 10))
    {
        deletedCellsLeft = false;

        forAll(nHatDeleted, iCell)
        {
            if (nHatDeleted[iCell])
            {
                bool found = false;

                //- check if nHat present in neighbour cells
                const labelList& cellCells = mesh_.cellCells()[iCell];
                forAll(cellCells, i)
                {
                    label neigh = cellCells[i];
                    if (magSqr(nHatCurvature_[neigh]) > SMALL)
                    {
                        nHatCurvature_[iCell] += nHatCurvature_[neigh];
                        found = true;
                    }
                }

                //- also check coupled boundaries
                const labelList& faces = mesh_.cells()[iCell];
                forAll(faces, i)
                {
                    label iFace = faces[i];
                    label iPatch = mesh_.boundaryMesh().whichPatch(iFace);
                    if (iPatch != -1)
                    {
                        if (
                            (alpha1_.boundaryField()[iPatch].type() == "processor")
                            || (alpha1_.boundaryField()[iPatch].type() == "cyclic")
                            || (alpha1_.boundaryField()[iPatch].type() == "cyclicAMI")
                        )
                        {
                            label iFaceLocal = iFace - mesh_.boundaryMesh()[iPatch].start();
                            vector nHatFace = nHatCurvature_.boundaryField()[iPatch][iFaceLocal];
                            if (magSqr(nHatFace) > SMALL)
                            {
                                nHatCurvature_[iCell] += nHatFace;
                                found = true;
                            }
                        }
                    }
                }

                if (found)
                {
                    nHatCurvature_[iCell] /= mag(nHatCurvature_[iCell]);
                    nHatDeleted[iCell] = false;
                }
                else
                {
                    //- loop has to be performed (at least) once again
                    deletedCellsLeft = true;
                }
            }
        }
        nHatCurvature_.correctBoundaryConditions();
        reduce(deletedCellsLeft, orOp<bool>());
        iLoop++;
    }
}






// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //




Foam::curvatureIsoSurface::curvatureIsoSurface
(
    const word& name,
    const volScalarField& alpha,
    const volVectorField& nHatv,
    const surfaceVectorField& nHatfv,
    const volScalarField& interfaceDensity,
    const List<bool>& isWallPatch,
    const dictionary& transpProp,
    const volScalarField& isInterface
)
    :
    curvature( name, alpha, nHatv, nHatfv, interfaceDensity, isWallPatch, transpProp, isInterface),

    nHatCurvature_
    (
       IOobject
       (
           "nHatCurvature",
           alpha.mesh().time().timeName(),
           alpha.mesh(),
           IOobject::READ_IF_PRESENT,
           IOobject::NO_WRITE //AUTO_WRITE
       ),
       alpha.mesh(),
       dimensionedVector("nHatCurvature", dimensionSet(0, 0, 0, 0, 0, 0, 0), vector (0, 0, 0))
    )

    
//    thinFilm_(transpProp.subDict("reconstruct").lookup("thinFilm")))
{
}



// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

void Foam::curvatureIsoSurface::calculateK()
{
   
/*   //Seems to have no Effect

    //1. Calculate Normal

    //- sets nHatCurvature_  only isoSurface-specific method!
    //  resets nHat only at wall boundaries
        prepareNHatCurv();

        boolList set1(mesh_.nCells(), false);
        boolList isInterfaceList1(mesh_.nCells(), false);

        forAll(set1, iCell)
        {
            if (interfaceDensity_[iCell] > VSMALL)
            {
                set1[iCell] = true;
            }
            isInterfaceList1[iCell] = isInterface_[iCell];
        }

        distributeField_.distributeVolField
        (
                nHatCurvature_,//field to distribute
                set1,          //list numbered as field with current distribution status
                isInterfaceList1,//list numbered as field with width of the destination field
                20,            //maxLoops
                0,             //minLoops
                true           //normalizeField
        );

        //- interpolate nHat for curvature calculation
        surfaceVectorField nHatfCurvature = fvc::interpolate(nHatCurvature_);

        //- copy contact angle correction from nHatfv_ on "real" boundaries
        forAll(nHatfCurvature.boundaryField(), iPatch)
        {
            if (
                (alpha1_.boundaryField()[iPatch].type() != "processor")
                && (alpha1_.boundaryField()[iPatch].type() != "cyclic")
                && (!alpha1_.boundaryField()[iPatch].fixesValue())
            )
            {
                nHatfCurvature.boundaryField()[iPatch] = nHatfv_.boundaryField()[iPatch];
            }
        }
 
*/
    
    //- calculate the curvature
    kappa_ = - fvc::div(nHatfv_ & mesh_.Sf());

    //- smooth the curvature and distribute it near the interface
    prePostSmooth(kappa_);

}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// ************************************************************************* //
