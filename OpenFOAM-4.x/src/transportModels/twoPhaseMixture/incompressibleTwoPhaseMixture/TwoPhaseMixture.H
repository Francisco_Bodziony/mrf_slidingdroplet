/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::TwoPhaseMixture

SourceFiles
    TwoPhaseMixture.C

Author
    Daniel Deising     <deising@mma.tu-darmstadt.de>
    All rights reserved.

Description
    A two-phase incompressible transportModel. Handles surface
    reconstruction (calculation of interface normals) and
    surface tension force calculation based on interface-library
    (classes reconstruct and curvature).

    You may refer to this software as :
    //- full bibliographic data to be provided

    This code has been developed by :
        Daniel Deising (main developer).
    
    Method Development and Intellectual Property :
        Daniel Deising   <deising@mma.tu-darmstadt.de>
        Holger Marschall <marschall@mma.tu-darmstadt.de>
        Dieter Bothe     <bothe@mma.tu-darmstadt.de>

        Mathematical Modeling and Analysis
        Center of Smart Interfaces
        Technische Universitaet Darmstadt
       
    If you use this software for your scientific work or your publications,
    please don't forget to acknowledge explicitly the use of it.

\*---------------------------------------------------------------------------*/

#ifndef TwoPhaseMixture_H
#define TwoPhaseMixture_H

#include "transportModel.H"
#include "viscosityModel.H"
#include "dimensionedScalar.H"
#include "volFields.H"
#include "surfaceFields.H"
#include "IOdictionary.H"

// additional headers for better curvature
#include "reconstruct.H"
#include "curvature.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                      Class TwoPhaseMixture Declaration
\*---------------------------------------------------------------------------*/

class TwoPhaseMixture
:
    public IOdictionary,
    public transportModel
{
protected:

    // Protected data

        word phase1Name_;
        word phase2Name_;

        autoPtr<viscosityModel> nuModel1_;
        autoPtr<viscosityModel> nuModel2_;

        dimensionedScalar rho1_;
        dimensionedScalar rho2_;

        const volVectorField& U_;
        const surfaceScalarField& phi_;

        const volScalarField& alpha1_;

        volScalarField nu_;
        
        //- Surface tension
        const dimensionedScalar sigma_;
        //- Stagnant Cap Model constant
        //const dimensionedScalar alphaSTF_;
        //- gravitational constant
        //dimensionedVector gravity_;
        //volScalarField K_;
        surfaceScalarField surfaceTensionForce_;

        //- interface auxiliary data
        const dimensionedScalar deltaN_;
        //surfaceScalarField etaf_;
        surfaceScalarField isInterfacef_;
        
    // from interfaceProperties:
        //- Compression coefficient
        scalar cAlpha_;
        
        
    // additional members for better curvature:
    
        //- indicates if an alpha1_ patch is a wall patch
        List<bool> isWallPatch_;
        
        //- markerfield for interface cells (1 true, 0 false)
        volScalarField isInterface_;
        
        //- define interface via snGradAlpha or alpha itself
        word isInterfaceMethod_;
        
        //- user defined value
        scalar isInterfaceThreshold_;
        
        //- thicken the interface by N cells
        int isInterfaceAddN_;
        
        //- model dependent reconstruction of the interface
        //  holds nHatv and nHatfv
        autoPtr<reconstruct> reconI_;
        
        //- model dependent curvature calculation
        autoPtr<curvature> K_;


    // Private Member Functions

        //- Calculate and return the laminar viscosity
        void calcNu();
        
        //- Re-calculate isInterface
        void calculateIsInterface();
        
        //- calculate indicator for stagnant cap model
        //volScalarField calculateIndicatorSTF();
        
        
    // additional member function for better curvature:
        List<bool> setIsWallPatch(const volScalarField& alpha) const;


public:

    // Constructors

        //- Construct from components
        TwoPhaseMixture
        (
            const volVectorField& U,
            const surfaceScalarField& phi,
            const word& alpha1Name = "alpha1"
        );


    //- Destructor
    ~TwoPhaseMixture()
    {}


    // Member Functions

        const word phase1Name() const
        {
            return phase1Name_;
        }

        const word phase2Name() const
        {
            return phase2Name_;
        }

        //- Return const-access to phase1 viscosityModel
        const viscosityModel& nuModel1() const
        {
            return nuModel1_();
        }

        //- Return const-access to phase2 viscosityModel
        const viscosityModel& nuModel2() const
        {
            return nuModel2_();
        }

        //- Return const-access to phase1 density
        const dimensionedScalar& rho1() const
        {
            return rho1_;
        }

        //- Return const-access to phase2 density
        const dimensionedScalar& rho2() const
        {
            return rho2_;
        };

        //- Return the dynamic laminar viscosity
        tmp<volScalarField> mu() const;

        //- Return the face-interpolated dynamic laminar viscosity
        tmp<surfaceScalarField> muf();

        //- Return the kinematic laminar viscosity
        virtual tmp<volScalarField> nu() const
        {
            return nu_;
        }
        
        //- Return the kinematic laminar viscosity
        virtual tmp<scalarField>  nu(const label patchi) const
        {
            return nu_.boundaryField()[patchi];
        }

        //- Return the face-interpolated kinematic laminar viscosity
        tmp<surfaceScalarField> nuf() const;
        
        //- Return the surface tension force
        tmp<volScalarField> sigmaK() const
        {
            return sigma_*K_->kappa();
        }
        
        //- Calculate the surface tension force
        //void calcStf();
        
        //- Better alternative: Return the surface tension force
        //const surfaceScalarField& surfaceTensionForce()
        //{
        //    return surfaceTensionForce_;
        //}
        tmp<surfaceScalarField> surfaceTensionForce();
        //{
        //    surfaceTensionForce_ = fvc::interpolate(sigmaK()) * fvc::snGrad(alpha1_);
        //    return surfaceTensionForce_;
        //}

        //- Correct the laminar viscosity
        virtual void correct()
        {
            calcNu();
            //- Calculate interface -> nHatv and nHatfv
            //  sets interfaceDensity
            reconI_->reconstructInterface();
            
            calculateIsInterface();
            
            //- Re-calculate curvature
            K_->calculateK();
        }
        
        
        //tmp<surfaceScalarField> etaf() DOES NOT WORK!!! ...
        // ... tmp calls the destructor for etaf_, thus deletes my private member!!!
        tmp<surfaceScalarField> etaf()
        {
            return reconI_->nHatf()/alpha1_.mesh().magSf();
        }
        
        const surfaceScalarField& isInterfacef()
        {
            return isInterfacef_;
        }
        
        const volScalarField& isInterface()
        {
            return isInterface_;
        }
        
        // return interfaceCompression-factor
        scalar cAlpha() const
        {
            return cAlpha_;
        }
        
        // return cell-face/interface orientation
        const surfaceScalarField& nHatf()
        {
            return reconI_->nHatf();
        }
        
        const volVectorField& nHatv()
        {
            return reconI_->nHatv();
        }
        
        const surfaceVectorField& nHatfv()
        {
            return reconI_->nHatfv();
        }
        
        const volScalarField& K()
        {
            return K_->kappa();
        }

        //- Read base transportProperties dictionary
        virtual bool read();
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
