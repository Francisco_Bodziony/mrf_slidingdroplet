/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::cstModels::harmonic

Description
    Use harmonic mean diffusion coeff to derive one-field CST equation

SourceFiles
    harmonic.C

\*---------------------------------------------------------------------------*/

#ifndef harmonic_H
#define harmonic_H

#include "cstModel.H"
#include "dimensionedScalar.H"
#include "volFieldsFwd.H"
#include "surfaceFieldsFwd.H"
#include "volFields.H"
#include "fvmDivCST.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
namespace cstModels
{

/*---------------------------------------------------------------------------*\
                           Class Newtonian Declaration
\*---------------------------------------------------------------------------*/

class harmonic
:
    public cstModel
{
//protected:
    //- number of fluxes needed for discretization
        static const label nFluxes_ = 2;
    
    //- calculate all needed fluxes on demand
        List<surfaceScalarField*> cstFluxes_;
        
        
    // Private Member Functions
    
        //- Return the pointer list to cstFluxes_ (lazy evaluation)
        void computeFluxes();
    
public:

    //- Runtime type information
    TypeName("harmonic");


    // Constructors

        //- construct from components
        harmonic
        (
            const word& name,
            const dictionary& speciesProperties,
            const volScalarField& alpha,
            const surfaceScalarField& etaf
        );


    //- Destructor
    ~harmonic()
    {
        // re-set cstFluxes to NULL (clean-up)
        for(label i = 0; i< nFluxes_; i++)
        {
            delete cstFluxes_[i];
        }
    }


    // Member Functions
        
        //- Return the interfacial species transfer term
        virtual tmp<fvScalarMatrix> interfaceTerm();

        //- Return the mean diffusion coefficient
        virtual tmp<surfaceScalarField> Dmean();
        
        //- Return the interfacial flux field for local Sherwood number calculations
        virtual tmp<surfaceScalarField> interfaceFlux();
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace cstModels
} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
