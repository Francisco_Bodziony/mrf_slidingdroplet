/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "blended.H"
#include "addToRunTimeSelectionTable.H"
#include "surfaceFields.H"
#include "fvc.H"
#include "fvmDivCST.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
namespace cstModels
{
    defineTypeNameAndDebug(blended, 0);
    addToRunTimeSelectionTable(cstModel, blended, dictionary);
}
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::cstModels::blended::blended
(
    const word& name,
    const dictionary& speciesProperties,
    const volScalarField& alpha,
    const surfaceScalarField& etaf
)
:
    cstModel(name, speciesProperties, alpha, etaf)
{}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

Foam::tmp<Foam::surfaceScalarField> Foam::cstModels::blended::interfaceFlux()
{
    const volScalarField& Ci(this->Ci_);
    const dimensionedScalar D1(this->D1_);
    const dimensionedScalar D2(this->D2_);
    const dimensionedScalar He(this->He_);
    
    volScalarField alpha(max(0.,min(this->alpha_,1.)));

    const fvMesh& mesh = alpha.mesh();    
    
    surfaceScalarField fluxDir = linearInterpolate(-fvc::grad(alpha)) & alpha.mesh().Sf();

    // prevent division by zero in case He=0!!
    //volScalarField denominatorV = alpha+ (1.-alpha)/He;
    volScalarField denominatorV = alpha*He + (1.-alpha);
    
    // stabilize in case He=0,alpha=1:
    if (He.value()==0)
    {
        denominatorV += 1e-10;
    }
    
    surfaceScalarField denominatorUpw =
        upwind<scalar>(mesh, fluxDir).interpolate
        (
            denominatorV
        );
    surfaceScalarField denominatorDow =
        downwind<scalar>(mesh, fluxDir).interpolate
        (
            denominatorV
        );

    surfaceScalarField Dmean(this->Dmean());
    
    surfaceScalarField phiCiIupw = 
    (
        -1/denominatorUpw
      //* Dmean * (1-1/He)
      * Dmean * (He - 1.)
      * fvc::snGrad(alpha)
    ) * mesh.magSf();

    surfaceScalarField phiCiIdow = 
    (
        -1/denominatorDow 
      //* Dmean * (1-1/He)
      * Dmean * (He - 1.)
      * fvc::snGrad(alpha)
    ) * mesh.magSf();

    surfaceScalarField upwindFlux = phiCiIupw * 
        upwind<scalar>(mesh, fluxDir).interpolate(Ci);

    surfaceScalarField downwindFlux = phiCiIdow * 
        downwind<scalar>(mesh, fluxDir).interpolate(Ci);
    
    return 0.5 * (upwindFlux + downwindFlux);
}


Foam::tmp<Foam::fvScalarMatrix> Foam::cstModels::blended::interfaceTerm()
{
    const volScalarField& Ci(this->Ci_);
    const dimensionedScalar D1(this->D1_);
    const dimensionedScalar D2(this->D2_);
    const dimensionedScalar He(this->He_);
    
    volScalarField alpha(max(0.,min(this->alpha_,1.))); //.oldTime()
    surfaceScalarField fluxDir = linearInterpolate(-fvc::grad(alpha)) & alpha.mesh().Sf();
    
    const fvMesh& mesh = alpha.mesh();
    
    const surfaceScalarField etaf(this->etaf_);
    
    surfaceScalarField snGradAlpha(fvc::snGrad(alpha));

    volScalarField denominatorV = alpha + (1.-alpha)/He;
    surfaceScalarField denominator = 
    (
        linearInterpolate(denominatorV)
    );
    surfaceScalarField denominatorUpw =
        upwind<scalar>(mesh, fluxDir).interpolate
        (
            denominatorV
        );
    surfaceScalarField denominatorDow =
        downwind<scalar>(mesh, fluxDir).interpolate
        (
            denominatorV
        );
        
    // calculate the harmonic mean diffusion coefficient
    surfaceScalarField Dharm(D1*D2/linearInterpolate(alpha*D2 + (scalar(1.)-alpha)*D1));

    surfaceScalarField phiCiIupwL = 
    (
        scalar(1.)/denominatorUpw
      * ( (D1-D2)/(He*denominator) - (D1-D2/He) )
      * snGradAlpha
    ) * mesh.magSf();

    surfaceScalarField phiCiIdowL = 
    (
        scalar(1.)/denominatorDow
      * ( (D1-D2)/(He*denominator) - (D1-D2/He) )
      * snGradAlpha
    ) * mesh.magSf();
    
    surfaceScalarField phiCiIupwH = 
    (
        -1/denominatorUpw
      * Dharm * (1-1/He)
      * snGradAlpha
    ) * mesh.magSf();

    surfaceScalarField phiCiIdowH = 
    (
        -1/denominatorDow 
      * Dharm * (1-1/He)
      * snGradAlpha
    ) * mesh.magSf();

    IStringStream upwindScheme("Gauss upwind"); 
    IStringStream downwindScheme("Gauss downwind"); 

    tmp<Foam::fv::convectionSchemeCST<scalar> > upwScheme(
        Foam::fv::convectionSchemeCST<scalar>::New(
            mesh,
            phiCiIupwL,
            fluxDir,
            upwindScheme
        )
    );
    tmp<Foam::fv::convectionSchemeCST<scalar> > dowScheme(
        Foam::fv::convectionSchemeCST<scalar>::New(
            mesh,
            phiCiIdowL,
            fluxDir,
            downwindScheme
        )
    );
    
    return 0.5*upwScheme().fvmDivCST(etaf*phiCiIupwH + (1-etaf)*phiCiIupwL, fluxDir, Ci)
          + 0.5*dowScheme().fvmDivCST(etaf*phiCiIdowH + (1-etaf)*phiCiIdowL, fluxDir, Ci);
}


Foam::tmp<Foam::surfaceScalarField> Foam::cstModels::blended::Dmean()
{
    const dimensionedScalar D1(this->D1_);
    const dimensionedScalar D2(this->D2_);
    const dimensionedScalar He(this->He_);
    const surfaceScalarField etaf(this->etaf_);
    
    volScalarField alpha(max(0.,min(this->alpha_,1.))); //.oldTime()
    
    tmp<surfaceScalarField> DiLinear = linearInterpolate(alpha*D1 + (scalar(1.)-alpha)*D2) 
        + (D1-D2)*linearInterpolate(alpha)*(1./linearInterpolate(alpha + (1.-alpha)/He) - 1.);
        
    tmp<surfaceScalarField> DiHarmonic = D1*D2/linearInterpolate(alpha*D2 + (scalar(1.)-alpha)*D1);
    
    return etaf * DiHarmonic + (1.-etaf) * DiLinear;
}

// ************************************************************************* //
