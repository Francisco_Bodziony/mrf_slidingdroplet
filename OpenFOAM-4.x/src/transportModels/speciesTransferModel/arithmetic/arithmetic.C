/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "arithmetic.H"
#include "addToRunTimeSelectionTable.H"
#include "surfaceFields.H"
#include "fvc.H"
#include "upwind.H"
#include "downwind.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
namespace cstModels
{
    defineTypeNameAndDebug(arithmetic, 0);
    addToRunTimeSelectionTable(cstModel, arithmetic, dictionary);
}
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::cstModels::arithmetic::arithmetic
(
    const word& name,
    const dictionary& speciesProperties,
    const volScalarField& alpha,
    const surfaceScalarField& etaf
)
:
    cstModel(name, speciesProperties, alpha, etaf),
    cstFluxes_(nFluxes_)
{
    // initialize pointers with zero
    for(label i = 0; i< nFluxes_; i++)
    {
        cstFluxes_[i] = nullptr;
    }
}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

void Foam::cstModels::arithmetic::computeFluxes()
{
// initialize list of flux fields if not yet existing
    for(label i = 0; i< nFluxes_; i++)
    {
        if (!cstFluxes_[i])
        {
            //Info<< "initialize cstFluxes pointer for species " << name_ << endl;
            cstFluxes_[i] = new surfaceScalarField
            (
                IOobject
                (
                    "flux" + Foam::name(i),
                    alpha_.time().timeName(),
                    alpha_.mesh(),
                    IOobject::NO_READ,
                    IOobject::NO_WRITE,
                    false // Do not register
                ),
                alpha_.mesh(),
                dimensionSet(0,1,-1,0,0)
            );
            
            //mesh.objectRegistry::store(cstFluxes_[i]);
        }
    }
    
// compute flux fields
    const dimensionedScalar D1(this->D1_);
    const dimensionedScalar D2(this->D2_);
    const dimensionedScalar He(this->He_);
    
    volScalarField alpha(max(0.,min(this->alpha_,1.)));
    
    const fvMesh& mesh = alpha.mesh();

    surfaceScalarField fluxDir = linearInterpolate(-fvc::grad(alpha)) & alpha.mesh().Sf();
    //surfaceScalarField fluxDir = this->fluxDir_;

    volScalarField denominatorV = alpha*He+ (1.-alpha);
    
    // prevent division by zero in case He=0!!
    if (He.value()==0)
    {
        denominatorV += 1e-10;
    }
    
    surfaceScalarField denominator = 
    (
        linearInterpolate(denominatorV)
    );
    surfaceScalarField denominatorUpw =
        upwind<scalar>(mesh, fluxDir).interpolate
        (
            denominatorV
        );
    surfaceScalarField denominatorDow =
        downwind<scalar>(mesh, fluxDir).interpolate
        (
            denominatorV
        );

    *cstFluxes_[0] = 
    (
        scalar(1.)/denominatorUpw
      * ( (D1-D2)/(denominator) - (D1*He-D2) )
      * fvc::snGrad(alpha)
    ); // * mesh.magSf();

    *cstFluxes_[1] = 
    (
        scalar(1.)/denominatorDow
      * ( (D1-D2)/(denominator) - (D1*He-D2) )
      * fvc::snGrad(alpha)
    ); // * mesh.magSf();
}


Foam::tmp<Foam::surfaceScalarField> Foam::cstModels::arithmetic::interfaceFlux()
{
    const volScalarField& Ci(this->Ci_);
    const dimensionedScalar D1(this->D1_);
    const dimensionedScalar D2(this->D2_);
    const dimensionedScalar He(this->He_);
    
    volScalarField alpha(max(0.,min(this->alpha_,1.)));
    
    const fvMesh& mesh = alpha.mesh();

    surfaceScalarField fluxDir = linearInterpolate(-fvc::grad(alpha)) & alpha.mesh().Sf();

    computeFluxes();

    // interpolation takes into account mesh grading at the interface
    surfaceScalarField delta = mesh.surfaceInterpolation::weights();
    
    // switch weights when face-parent is not upwind cell!
    forAll(fluxDir, f)
    {
        if (fluxDir[f] < 0)
        {
            delta[f] = 1-delta[f];
        }
    }
    
    surfaceScalarField upwindFlux = *cstFluxes_[0] * 
        upwind<scalar>(mesh, fluxDir).interpolate(Ci);

    surfaceScalarField downwindFlux = *cstFluxes_[1] * 
        downwind<scalar>(mesh, fluxDir).interpolate(Ci);

    // re-set cstFluxes to NULL (clean-up)
    for(label i = 0; i< nFluxes_; i++)
    {
        cstFluxes_[i] = nullptr;
    }

    return 0.5*(delta*upwindFlux + (1.-delta)*downwindFlux);
}


Foam::tmp<Foam::fvScalarMatrix> Foam::cstModels::arithmetic::interfaceTerm()
{
    const volScalarField& Ci(this->Ci_);
    const dimensionedScalar D1(this->D1_);
    const dimensionedScalar D2(this->D2_);
    const dimensionedScalar He(this->He_);
    
    volScalarField alpha(max(0.,min(this->alpha_,1.)));
    
    const fvMesh& mesh = alpha.mesh();

    surfaceScalarField fluxDir = linearInterpolate(-fvc::grad(alpha)) & alpha.mesh().Sf();
    //surfaceScalarField fluxDir = this->fluxDir_;

    computeFluxes();

    surfaceScalarField phiCiIupw = 
    (
        *cstFluxes_[0]
    ) * mesh.magSf();

    surfaceScalarField phiCiIdow = 
    (
        *cstFluxes_[1]
    ) * mesh.magSf();

    IStringStream upwindScheme("Gauss upwind"); 
    IStringStream downwindScheme("Gauss downwind"); 

    tmp<Foam::fv::convectionSchemeCST<scalar> > upwScheme(
        Foam::fv::convectionSchemeCST<scalar>::New(
            mesh,
            phiCiIupw,
            fluxDir,
            upwindScheme
        )
    );
    tmp<Foam::fv::convectionSchemeCST<scalar> > dowScheme(
        Foam::fv::convectionSchemeCST<scalar>::New(
            mesh,
            phiCiIdow,
            fluxDir,
            downwindScheme
        )
    );
    
    // interpolation takes into account mesh grading at the interface
    surfaceScalarField delta = mesh.surfaceInterpolation::weights();
    
    // switch weights when face-parent is not upwind cell!
    forAll(fluxDir, f)
    {
        if (fluxDir[f] < 0)
        {
            delta[f] = 1-delta[f];
        }
    }

    for(label i = 0; i< nFluxes_; i++)
    {
        cstFluxes_[i] = nullptr;
    }

    return upwScheme().fvmDivCST(delta*phiCiIupw, fluxDir, Ci)
         +  dowScheme().fvmDivCST((1.-delta)*phiCiIdow, fluxDir, Ci);
}


Foam::tmp<Foam::surfaceScalarField> Foam::cstModels::arithmetic::Dmean()
{
    const dimensionedScalar D1(this->D1_);
    const dimensionedScalar D2(this->D2_);
    const dimensionedScalar He(this->He_);
    
    volScalarField alpha(max(0.,min(this->alpha_,1.)));

    // this only works for He != 0 and Di != 0 !!
    surfaceScalarField denominator(linearInterpolate(alpha*He + (1.-alpha)));
    // stabilize in case He=0!
    if (He.value()==0)
    {
        denominator += 1e-10;
    }
    return linearInterpolate(alpha*D1 + (scalar(1.)-alpha)*D2) 
        + (D1-D2)*linearInterpolate(alpha)*(He/denominator - 1.);
}


// ************************************************************************* //
