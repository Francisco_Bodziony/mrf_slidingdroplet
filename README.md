# README #

This README documents the necessary steps to use this library.

### What is this repository for? ###

* Simulation of interfacial mass transfer with chemical reactions:
    * calculates species transfer of a dilute species (no volume effects)
    * considers chemical reactions in the bulk of one phase only
    * contains specialized solvers for the simulation of single rising bubbles (Moving Reference Frame) and bubble swarms (Center-of-Momentum Reference Frame)
    * Library supports local adaptive mesh refinement and dynamic load balancing with multi-criterion refinement
    * contains a library of High Resolution Interface Capturing Schemes (CICSAM. HRIC, interGamma, M-CICSAM, M-HRIC)
* Version: the newest developments compile only against OpenFOAM-4.x

### How do I get set up? ###

* Simply source your OpenFOAM environment variables and then run the ./Allwmake script in the OpenFOAM-4.x folder
* In case you wish to compile single libs or apps yourself, source the local etc/bashrc first

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Contributions contact Repo owner (email)
* Other community or team contact
